# Measure Docker Build durations

> README IS STILL WIP

Sometimes docker builds can take forever. That can be annoying when you have to rebuild the image very often.
But docker offers a great solution to minimize the build time -> CACHING.

To optimize your builds it can help that you know which step needs a lot of time. This small cli
helps you to measure the duration which each step requires.

# Installation

You can install it global or locally. I recommend the global installation.
```
$ npm i -g measuredockerbuild
```
```
$ npm i measuredockerbuild
```

# Usage

```bash
$ measuredockerbuild <json|table> <docker build args>
```

e.g.
```bash
$ measuredockerbuild table -t ionic_conference_app:latest .
```

# Additional
- Feel free to open an issue if you have problems. [Link to issues](https://gitlab.com/milleniumfrog/measuredockerbuild/-/issues)
