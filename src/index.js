const process = require('process');
const child_process = require('child_process');
const { fstat } = require('fs');

// remove args that tell which program is used
const measureDockerBuildProgrammIndex = process.argv.findIndex(arg => arg.endsWith('measuredockerbuild'));
const outPutType = process.argv[measureDockerBuildProgrammIndex+1];
const buildCommand = `docker build ${process.argv.slice(measureDockerBuildProgrammIndex+2).join(' ')}`;

if (outPutType !== 'json' && outPutType !== 'table') {
    console.info('usage: measuredockerbuild <json|table> [docker build args]');
    console.info('e.g. measuredockerbuild json -t buildlabel .');
    return;
}

console.info(`Measure time for command: ${buildCommand}`);

const rawTimeData = [{
    step: 'START BUILD',
    timeStamp: Date.now(),
}];

const dockerBuildProcess = child_process.exec(buildCommand);

dockerBuildProcess.stdout.on('data', (chunk) => {

    console.log(chunk);

    const lines = chunk.split('\n');
    const stepList = lines.filter((l) => l.startsWith('Step'));
    for (const step of stepList) {
        rawTimeData.push({
            step,
            timeStamp: Date.now(),
        })
    }

});

dockerBuildProcess.stderr.on('data', (chunk) => console.error(chunk));

dockerBuildProcess.on('exit', () => {

    rawTimeData.push({
        step: 'END BUILD',
        timeStamp: Date.now(),
    });

    const durationArray =  rawTimeData.map(({step, timeStamp}, index) => {
        if (index < rawTimeData.length -1) {
            return {step, duration: rawTimeData[index+1].timeStamp - timeStamp};
        }
        else {
            const latestIndex = rawTimeData.length-1;
            return {step, duration: rawTimeData[latestIndex].timeStamp - rawTimeData[0].timeStamp};
        }
    });

    if (outPutType === 'json') {
        console.log(durationArray);
    }
    else {
        const maxStepLength = durationArray.reduce((acc, curr) => curr.step.length > acc ? curr.step.length : acc, 0);
        for (const durationObj of durationArray) {
            console.log(
                durationObj.step, 
                '\ '.repeat(maxStepLength-durationObj.step.length + 3),
                '|',
                durationObj.duration,
                'ms'
            );
            console.log('-'.repeat(maxStepLength + 20));
        }
    }

})